const express = require('express');
const router = express.Router();
const con = require("../db")

router.get("/timeSlots/",(req,res)=>{
    const barbars =req.body.barbars;
    const date = req.body.date;
    const data = [barbars, date]
    var barbarArray ="("
    for (key in barbars) {
        if (key==barbars.length-1) {
            barbarArray += barbars[key]
        } else {
            barbarArray += barbars[key] +","
        }
    }
    barbarArray += ")"
        const sql = "select * from bookingDetails where staffID in"+barbarArray+"and date = ?"
        con.query(sql,date, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })    
    
})

router.get("/",(req,res)=>{
    con.query("select * from bookingDetails", (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.get("/:id",(req,res)=>{
    con.query("select * from bookingDetails where bookingDetailsID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result[0])
            res.send(result[0])
        }
    })
})

router.post("/",(req,res)=>{
    const staffID = req.body.staffID;
    const serviceID = req.body.serviceID;
    const date = req.body.date;
    const time = req.body.time;
    const comment = req.body.comment;
    const rating = req.body.rating;
    const data = [staffID, serviceID, date, time, comment, rating]
    const sql = "insert into bookingDetails (staffID, serviceID, date, time, comment, rating) values (?,?,?,?,?,?)";
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.put("/:id",(req,res)=>{
    const staffID = req.body.staffID;
    const serviceID = req.body.serviceID;
    const date = req.body.date;
    const time = req.body.time;
    const comment = req.body.comment;
    const rating = req.body.rating;
    const bookingDetailsID= req.params.id;	
    const data = [staffID, serviceID, date, time, comment, rating, bookingDetailsID]
    const sql = "update bookingDetails set staffID=?, serviceID=?, date=?, time=?, comment=?, rating=? where bookingDetailsID=?"
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.delete("/:id",(req,res)=>{
    con.query("delete from bookingDetails where bookingDetailsID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

module.exports = router