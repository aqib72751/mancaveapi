const express = require("express");
const router = express.Router();
const con = require("../db");

router.get("/", (req, res) => {
  con.query("select s.staffID, s.firstName, s.lastName, s.profile, s.rating, s.speciality, s.description, s.status, s.flag,s.dayOff, f.country, f.flagURL from staff s join flags f on s.flag = f.flagID", (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });


});

router.get("/:id", (req, res) => {
  con.query(
    "select * from staff where staffID=?",
    [req.params.id],
    (error, result) => {
      if (error) {
        console.log(error);
        res.send(error);
      } else {
        console.log(result[0]);
        res.send(result[0]);
      }
    }
  );
});

router.post("/", (req, res) => {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const country = req.body.country;
  const flag = req.body.flag;
  const profile = req.body.profile;
  const rating = req.body.rating;
  const speciality = req.body.speciality;
  const description = req.body.description;
  const status = req.body.status;
  const dayOff = req.body.dayOff;

  const data = [
    firstName,
    lastName,
    country,
    flag,
    profile,
    rating,
    speciality,
    description,
    status,
    dayOff
  ];
  const sql =
    "insert into staff (firstName, lastName, country, flag, profile, rating, speciality, description, status, dayOff) values (?,?,?,?,?,?,?,?,?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

router.put("/:id", (req, res) => {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const country = req.body.country;
  const flag = req.body.flag;
  const profile = req.body.profile;
  const rating = req.body.rating;
  const speciality = req.body.speciality;
  const description = req.body.description;
  const status = req.body.status;
  const dayOff = req.body.dayOff;
  const staffID = req.params.id;
  const data = [
    firstName,
    lastName,
    country,
    flag,
    profile,
    rating,
    speciality,
    description,
    status,
    dayOff,
    staffID,
  ];
  const sql =
    "update staff set firstName=?, lastName=?, country=?, flag=?, profile=?, rating=?, speciality=?, description=?, status=?, dayOff=? where staffID=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

router.delete("/:id", (req, res) => {
  con.query(
    "delete from staff where staffID=?",
    [req.params.id],
    (error, result) => {
      if (error) {
        console.log(error);
        res.send(error);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});

module.exports = router;
