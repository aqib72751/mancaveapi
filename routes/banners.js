const express = require('express');
const router = express.Router();
const con = require("../db")

router.get("/",(req,res)=>{
    con.query("select * from banners", (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.get("/:id",(req,res)=>{
    con.query("select * from banners where bannerID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result[0])
            res.send(result[0])
        }
    })
})

router.post("/",(req,res)=>{
    const URL = req.body.URL;
    const data = [URL]
    const sql = "insert into banners (URL) values (?)";
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.put("/:id",(req,res)=>{
    const URL = req.body.URL;
    const bannerID= req.params.id;	
    const data = [URL, bannerID]
    const sql = "update banners set URL=? where bannerID=?"
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.delete("/:id",(req,res)=>{
    con.query("delete from banners where bannerID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

module.exports = router