const express = require("express");
const router = express.Router();
const con = require("../db");
const app = express();


router.get("/", (req, res) => {
  con.query("select userID, name, email, contact from users where role='user'", (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      //console.log(result);
      res.send(result);
    }
  });
});

router.post("/login", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const data = [username, password];
  con.query(
    "select * from users where email=? and password=?",
    data,
    (error, result) => {
      if (error) {
        console.log(error);
        res.send(error);
      } else {
        var ress = result[0];
        if (res === null || res === "" || res === undefined) {
          res.send({
            status: "404",
            data: "No data found",
          });
        } else {
          res.send({
            status: "success",
            data: result[0],
          });
        }
      }
    }
  );
});

router.post("/", (req, res) => {
  const name = req.body.name;
  const email = req.body.email;
  const password = req.body.password;
  const lastLoginTime = null;
  const verificationCode = null;
  const status = "active";
  const contact = req.body.contact;
  const role = req.body.role;
  const deviceID = req.body.deviceID;
  const data = [
    name,
    email,
    password,
    lastLoginTime,
    verificationCode,
    status,
    contact,
    role,
    deviceID,
  ];
  const sql =
    "insert into users (name, email, password, lastLoginTime, verificationCode,status,contact,role,deviceID) values (?,?,?,?,?,?,?,?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(result);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

router.put("/:id", (req, res) => {
  const name = req.body.name;
  const email = req.body.email;
  const password = req.body.password;
  const lastLoginTime = req.body.lastLoginTime;
  const status = req.body.status;
  const contact = req.body.contact;
  const deviceID = req.body.deviceID;
  const userID = req.params.id;
  const data = [name, email, password, lastLoginTime, status, contact,deviceID, userID];
  const sql =
    "update users set name=?, email=?, password=?, lastLoginTime=?, status=?, contact=?,deviceID=? where userID=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
    } else {
      res.send(result);
    }
  });
});

router.delete("/:id", (req, res) => {
  con.query(
    "delete from users where userID=?",
    [req.params.id],
    (error, result) => {
      if (error) {
        console.log(error);
      } else {
        res.send(result);
      }
    }
  );
});

router.post("/reset", (req, res) => { 
  var html = "'<!DOCTYPE html>\n<html >\n  <head>\n    <meta charset=\'UTF-8\'>\n    <title>Your Jet password has been updated</title>\n    \n    \n    \n    \n    \n    \n    \n    \n  </head>\n\n  <body>\n\n    <!DOCTYPE html PUBLIC \'-//W3C//DTD XHTML 1.0 Transitional//EN\' \'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\'>\n<html xmlns=\'http://www.w3.org/1999/xhtml\'>\n\n<head>\n  <meta http-equiv=\'Content-Type\' content=\'text/html; charset=UTF-8\'>\n  <meta name=\'viewport\' content=\'width=device-width\'>\n  <meta name=\'format-detection\' content=\'telephone=no\'>\n  <!--[if !mso]>\n      <!-->\n  <link href=\'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300&subset=latin\' rel=\'stylesheet\' type=\'text/css\'>\n  <!--<![endif]-->\n  <title>Jet.com - Password Reset</title>\n  <style type=\'text/css\'>\n    *{\n      \t\t\tmargin:0;\n      \t\t\tpadding:0;\n      \t\t\tfont-family:'OpenSans-Light', \'Helvetica Neue\', \'Helvetica\',Calibri, Arial, sans-serif;\n      \t\t\tfont-size:100%;\n      \t\t\tline-height:1.6;\n      \t\t}\n      \t\timg{\n      \t\t\tmax-width:100%;\n      \t\t}\n      \t\tbody{\n      \t\t\t-webkit-font-smoothing:antialiased;\n      \t\t\t-webkit-text-size-adjust:none;\n      \t\t\twidth:100%!important;\n      \t\t\theight:100%;\n      \t\t}\n      \t\ta{\n      \t\t\tcolor:#348eda;\n      \t\t}\n      \t\t.btn-primary{\n      \t\t\ttext-decoration:none;\n      \t\t\tcolor:#FFF;\n      \t\t\tbackground-color:#a55bff;\n      \t\t\tborder:solid #a55bff;\n      \t\t\tborder-width:10px 20px;\n      \t\t\tline-height:2;\n      \t\t\tfont-weight:bold;\n      \t\t\tmargin-right:10px;\n      \t\t\ttext-align:center;\n      \t\t\tcursor:pointer;\n      \t\t\tdisplay:inline-block;\n      \t\t}\n      \t\t.last{\n      \t\t\tmargin-bottom:0;\n      \t\t}\n      \t\t.first{\n      \t\t\tmargin-top:0;\n      \t\t}\n      \t\t.padding{\n      \t\t\tpadding:10px 0;\n      \t\t}\n      \t\ttable.body-wrap{\n      \t\t\twidth:100%;\n      \t\t\tpadding:0px;\n      \t\t\tpadding-top:20px;\n      \t\t\tmargin:0px;\n      \t\t}\n      \t\ttable.body-wrap .container{\n      \t\t\tborder:1px solid #f0f0f0;\n      \t\t}\n      \t\ttable.footer-wrap{\n      \t\t\twidth:100%;\n      \t\t\tclear:both!important;\n      \t\t}\n      \t\t.footer-wrap .container p{\n      \t\t\tfont-size:12px;\n      \t\t\tcolor:#666;\n      \t\t}\n      \t\ttable.footer-wrap a{\n      \t\t\tcolor:#999;\n      \t\t}\n      \t\t.footer-content{\n      \t\t\tmargin:0px;\n      \t\t\tpadding:0px;\n      \t\t}\n      \t\th1,h2,h3{\n      \t\t\tcolor:#660099;\n      \t\t\tfont-family:'OpenSans-Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;\n      \t\t\tline-height:1.2;\n      \t\t\tmargin-bottom:15px;\n      \t\t\tmargin:40px 0 10px;\n      \t\t\tfont-weight:200;\n      \t\t}\n      \t\th1{\n      \t\t\tfont-family:'Open Sans Light';\n      \t\t\tfont-size:45px;\n      \t\t}\n      \t\th2{\n      \t\t\tfont-size:28px;\n      \t\t}\n      \t\th3{\n      \t\t\tfont-size:22px;\n      \t\t}\n      \t\tp,ul,ol{\n      \t\t\tmargin-bottom:10px;\n      \t\t\tfont-weight:normal;\n      \t\t\tfont-size:14px;\n      \t\t}\n      \t\tul li,ol li{\n      \t\t\tmargin-left:5px;\n      \t\t\tlist-style-position:inside;\n      \t\t}\n      \t\t.container{\n      \t\t\tdisplay:block!important;\n      \t\t\tmax-width:600px!important;\n      \t\t\tmargin:0 auto!important;\n      \t\t\tclear:both!important;\n      \t\t}\n      \t\t.body-wrap .container{\n      \t\t\tpadding:0px;\n      \t\t}\n      \t\t.content,.footer-wrapper{\n      \t\t\tmax-width:600px;\n      \t\t\tmargin:0 auto;\n      \t\t\tpadding:20px 33px 20px 37px;\n      \t\t\tdisplay:block;\n      \t\t}\n      \t\t.content table{\n      \t\t\twidth:100%;\n      \t\t}\n      \t\t.content-message p{\n      \t\t\tmargin:20px 0px 20px 0px;\n      \t\t\tpadding:0px;\n      \t\t\tfont-size:22px;\n      \t\t\tline-height:38px;\n      \t\t\tfont-family:'OpenSans-Light',Calibri, Arial, sans-serif;\n      \t\t}\n      \t\t.preheader{\n      \t\t\tdisplay:none !important;\n      \t\t\tvisibility:hidden;\n      \t\t\topacity:0;\n      \t\t\tcolor:transparent;\n      \t\t\theight:0;\n      \t\t\twidth:0;\n      \t\t}\n  </style>\n</head>\n\n<body bgcolor=\'#f6f6f6\'>\n  <span class=\'preheader\' style=\'display: none !important; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;\'>\n    You’re back in the game\n</span>\n\n  <!-- body -->\n  <table class=\'body-wrap\' width=\'600\'>\n    <tr>\n      <td class=\'container\' bgcolor=\'#FFFFFF\'>\n        <!-- content -->\n        <table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' class=\'contentwrapper\' width=\'600\'>\n          <tr>\n            <td style=\'height:25px;\'>\n              <img border=\'0\' src=\'https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/96288204-f67c-4ba2-9981-1be77c9fa18b.png\' width=\'600\'>\n            </td>\n          </tr>\n          <tr>\n            <td>\n              <div class=\'content\'>\n                <table class=\'content-message\'>\n                  <tr>\n                    <td>&nbsp;</td>\n                  </tr>\n                  <tr>\n                    <td align=\'left\'>\n                      <a href=\'https://jet.com\'>\n                        <img src=\'https://gallery.mailchimp.com/30e2f3de9299bd9452026da4a/images/d7750da0-5a18-4785-9cac-0cefbb49883b.png\' width=\'126\' border=\'0\'>\n                      </a>\n                    </td>\n                  </tr>\n                  <tr>\n                    <td class=\'content-message\' style=\'font-family:'Open Sans Light',Calibri, Arial, sans-serif; color: #595959;\'>\n                      <p>&nbsp;</p>\n                      <p>\n                        <img width=\'190\' height=\'65\' src=\'https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/70ea32b8-20b3-4eb9-88d5-72089d8129d3.png\' alt=\'Your new Jet password\' border=\'0\'>\n                      </p>\n                      <h1 style=\'font-family:'OpenSans-Light', Helvetica, Calibri, Arial, sans-serif;\'>\n                                            Your new Jet password\n                                        </h1>\n\n                      <p style=\'font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;\'>Your Jet password has been updated and it works like a charm. Nice job.</p>\n                      <p style=\'font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;\'>&nbsp;</p>\n                      <table width=\'325\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'>\n                        <tr>\n                          <td width=\'325\' height=\'60\' bgcolor=\'#31cccc\' style=\'text-align:center;\'>\n                            <a href=\'https://jet.com?jcmp=em:tx:PasswordUpdated:tx:na:tx:takemetojet:na:tx:1\' align=\'center\' style=\'display:block; font-family:'Open Sans',Calibri, Arial, sans-serif;; font-size:20px; color:#ffffff; text-align: center; line-height:60px; display:block; text-decoration:none;\'>Take me to Jet</a>\n                          </td>\n                          <td>&nbsp;</td>\n                          <td>&nbsp;</td>\n                        </tr>\n                      </table>\n                      <p style=\'font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;\'>\n                        <img border=\'0\' src=\'https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/4d4431c8-e778-47ac-a026-a869106b2903.gif\' height=\'50\' width=\'200\'>\n                      </p>\n                      <p style=\'font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;\'>If you didn’t just reset your password, get in touch with the Jet Heads so we can help secure your account. Call us at\n                        <a href=\'tel:1-855-538-4323\' style=\'color:#33CCCC; font-weight:bold; text-decoration:none;\'>1 (855) JET HEADS (538 4323)</a>or email us at\n                        <a href=\'mailto:help@jet.com\' style=\'color:#33CCCC; font-weight:bold; text-decoration:none; \'>help@jet.com</a>.</p>\n                      <p style=\'font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;\'>\n                        <img border=\'0\' src=\'https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/4d4431c8-e778-47ac-a026-a869106b2903.gif\' height=\'50\' width=\'200\'>\n                      </p>\n                    </td>\n                  </tr>\n                </table>\n              </div>\n            </td>\n          </tr>\n          <tr>\n            <td bgcolor=\'#F7F7F7\' style=\'max-width:600px; margin:0 auto; padding:20px 33px 20px 37px; display:block;\'>\n              <table cellspacing=\'0\' cellpadding=\'10\' width=\'100%\'>\n                <tr>\n                  <td colspan=\'3\'>\n                    <table cellspacing=\'0\' cellpadding=\'10\' width=\'100%\'>\n                      <tr>\n                        <td style=\'font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #A55BFF; text-align:left;\' align=\'center\'>Free shipping over $35</td>\n                        <td style=\'width:20px; font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #A55BFF; text-align:center;\' align=\'center\'>&#9679;</td>\n                        <td style=\'font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #A55BFF; text-align:center;\' align=\'center\'>Free returns up to 30 days</td>\n                        <td style=\'width:20px; font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #A55BFF; text-align:center;\' align=\'center\'>&#9679;</td>\n                        <td style=\'font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #A55BFF; text-align:right;\' align=\'center\'>24/7 Customer Service</td>\n                      </tr>\n                    </table>\n                  </td>\n                </tr>\n                <tr>\n                  <td colspan=\'3\' height=\'18\' style=\'font-size:1px; line-height:1px;\'>&nbsp;</td>\n                </tr>\n                <tr>\n                  <td align=\'left\' colspan=\'3\'>\n                    <table cellpadding=\'0\' cellspacing=\'0\' width=\'100%\'>\n                      <tr>\n                        <td>\n                          <table>\n                            <tr>\n                              <td>\n                                <div style=\'float:left; margin:5px;\'>\n                                  <a href=\'https://www.facebook.com/jet\' title=\'Like us on Facebook\' target=\'new\'>\n                                    <img src=\'https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/3f540d56-178e-4d73-83e5-8ccaaf70f2cd.png\' width=\'29\' height=\'29\' border=\'0\' alt=\'Like us on Facebook\'>\n                                  </a>\n                                </div>\n                              </td>\n                              <td>\n                                <div style=\'float:left; margin:5px;\'>\n                                  <a href=\'https://twitter.com/jet\' title=\'Jet on Twitter\' target=\'new\'>\n                                    <img src=\'https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/e78ba6bd-5b0f-4d69-8fcf-acc064cbf7ea.png\' width=\'29\' height=\'29\' border=\'0\' alt=\'Jet on Twitter\'>\n                                  </a>\n                                </div>\n                              </td>\n                              <td>\n                                <div style=\'float:left; margin:5px;\'>\n                                  <a href=\'https://www.instagram.com/jet/\' title=\'Jet on Instagram\' target=\'new\'>\n                                    <img src=\'https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/9f72bdd3-5361-4f76-b683-6633a1c29145.png\' width=\'29\' height=\'29\' border=\'0\' alt=\'Jet on Instagram\'>\n                                  </a>\n                                </div>\n                              </td>\n                              <td>\n                                <div style=\'float:left; margin:5px;\'>\n                                  <a href=\'https://www.linkedin.com/company/jet-com\' title=\'Jet on LinkedIn\' target=\'new\'>\n                                    <img src=\'https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/3a1bd7ef-5fd9-42ab-842f-bb4380609673.jpg\' width=\'29\' height=\'29\' border=\'0\' alt=\'Jet on LinkedIn\'>\n                                  </a>\n                                </div>\n                              </td>\n                            </tr>\n                          </table>\n                        </td>\n                        <td align=\'right\' style=\'text-align:right;\'>\n                          <a href=\'mailto:help@jet.com\' style=\'text-decoration: none; font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;\' title=\'help@jet.com\'>help@jet.com</a>                          <span style=\'text-decoration: none; font-size:9px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;\'>|</span>\n\n                          <a style=\'text-decoration: none; font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;\' href=\'tel:18555384323\'>1 (855) JET HEADS (538 4323)</a>\n                        </td>\n                      </tr>\n                    </table>\n                  </td>\n                </tr>\n                <tr>\n                  <td colspan=\'3\' height=\'18\' style=\'font-size:1px; line-height:1px;\'>&nbsp;</td>\n                </tr>\n                <tr>\n                  <td style=\'font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141; text-align:left;\' align=\'left\'>\n                    <a href=\'https://jet.com/privacy-policy\' style=\'text-decoration:none;font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141;\'>Privacy Policy</a>\n                  </td>\n                  <td colspan=\'2\' style=\'font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141; text-align:right;\' align=\'right\'>221 River Street, 8th Floor, Hoboken, NJ 07030 &copy; 2016 Jet</td>\n                </tr>\n              </table>\n            </td>\n          </tr>\n          <tr>\n            <td style=\'height:25px;\'>\n              <img width=\'600\' src=\'https://gallery.mailchimp.com/d42c37cf5f5c0fac90b525c8e/images/4c1b3727-e048-4e80-815b-a9197acc62fe.png\'>\n            </td>\n          </tr>\n          <tr>\n            <td>\n              <table border=\'0\' cellspacing=\'0\' cellpadding=\'0\'>\n                <tr>\n                  <td colspan=\'3\'>&nbsp;</td>\n                </tr>\n                <tr>\n                  <td width=\'25\'></td>\n                  <td> <span style=\'font-size:8px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #535352;\'>\n                                    Jet.com only ships to the 48 contiguous U.S. states and the District of Columbia. To qualify for free shipping, the order merchandise total before savings and estimated tax must be at least $35. Multiple orders cannot be combined to reach the $35. For orders less than $35, there will be a $5.99 shipping charge. Returns must be initiated within 30 days of receiving items. Free returns not available if you waive returns in exchange for savings. Items returned must be in original condition with all packaging and tags.\n                                    </span>\n\n                  </td>\n                  <td width=\'25\'></td>\n                </tr>\n                <tr>\n                  <td colspan=\'3\'>&nbsp;</td>\n                </tr>\n                <tr>\n                  <td colspan=\'3\'>&nbsp;</td>\n                </tr>\n              </table>\n            </td>\n          </tr>\n        </table>\n        <!-- /content -->\n      </td>\n      <td></td>\n    </tr>\n  </table>\n  <!-- /body -->\n</body>\n\n</html>\n    \n    \n    \n    \n    \n  </body>\n</html>\n'";
  const { emailTo, clientKey, secretKey } = req.body;
  console.log("Reset....");
  const mailjet = require("node-mailjet").connect(
    "aea44ec9695f2227fc0b179c02de1fe4",
    "f7b3136295577ac0baa6060578d59945"
  );
  const request = mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "appick.io@gmail.com",
          Name: "Appick",
        },
        To: [
          {
            Email: emailTo,
            Name: "Man cave user",
          },
        ],
        Subject: "Man Cave - Password reset link",        
        HTMLPart: html,
      },
    ],
  });
  request
    .then((result) => {
      console.log(result.body);
      res.json({ status: "success", data: result.body });
    })
    .catch((err) => {
      console.log(err.statusCode);
      res.json({ status: "error", data: err });
    });
});

module.exports = router;
