const express = require("express");
const router = express.Router();
const con = require("../db");

router.get("/", (req, res) => {
  con.query("select * from flags", (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

router.get("/:id", (req, res) => {
  con.query(
    "select * from flags where flagID=?",
    [req.params.id],
    (error, result) => {
      if (error) {
        console.log(error);
        res.send(error);
      } else {
        console.log(result[0]);
        res.send(result[0]);
      }
    }
  );
});

router.post("/", (req, res) => {
  const country = req.body.country;
  const flagURL = req.body.flagURL;
  const data = [country, flagURL];
  const sql = "insert into flags (country, flagURL) values (?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

router.put("/:id", (req, res) => {
  const country = req.body.country;
  const flagURL = req.body.flagURL;
  const flagID = req.params.id;
  const data = [country, flagURL, flagID];
  const sql =
    "update flags set country=?, flagURL=? where flagID=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

router.delete("/:id", (req, res) => {
  con.query(
    "delete from flags where flagID=?",
    [req.params.id],
    (error, result) => {
      if (error) {
        console.log(error);
        res.send(error);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});

module.exports = router;
