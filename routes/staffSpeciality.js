const express = require("express");
const router = express.Router();
const con = require("../db");

router.get("/", (req, res) => {
  con.query("select * from staffSpeciality", (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

router.get("/:id", (req, res) => {
  con.query(
    "select * from staffSpeciality where staffSpecialityID=?",
    [req.params.id],
    (error, result) => {
      if (error) {
        console.log(error);
        res.send(error);
      } else {
        console.log(result[0]);
        res.send(result[0]);
      }
    }
  );
});

router.post("/", (req, res) => {
  const staffID = req.body.staffID;
  const serviceID = req.body.serviceID;
  const data = [staffID, serviceID];
  const sql =
    "insert into staffSpeciality (staffID, serviceID) values (?,?)";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

router.put("/:id", (req, res) => {
  const staffID = req.body.staffID;
  const serviceID = req.body.serviceID;
  const staffSpecialityID = req.params.id;
  const data = [
    staffID,
    serviceID,
    staffSpecialityID,
  ];
  const sql =
    "update staffSpeciality set staffID=?, serviceID=? where staffSpecialityID=?";
  con.query(sql, data, (error, result) => {
    if (error) {
      console.log(error);
      res.send(error);
    } else {
      console.log(result);
      res.send(result);
    }
  });
});

router.delete("/:id", (req, res) => {
  con.query(
    "delete from staffSpeciality where staffSpecialityID=?",
    [req.params.id],
    (error, result) => {
      if (error) {
        console.log(error);
        res.send(error);
      } else {
        console.log(result);
        res.send(result);
      }
    }
  );
});

module.exports = router;
