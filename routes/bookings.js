const express = require('express');
const router = express.Router();
const con = require("../db")

router.get("/",(req,res)=>{
    con.query("select * from bookings", (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.get("/:id",(req,res)=>{
    con.query("select * from bookings where bookingID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result[0])
            res.send(result[0])
        }
    })
})

router.post("/",(req,res)=>{
    const userID = req.body.userID;
    const staffID = req.body.staffID;
    const serviceID = req.body.serviceID;
    const transactionID = req.body.transactionID;
    const date = req.body.date;
    const time = req.body.time;
    const bill = req.body.bill;
    const tax = req.body.tax;
    const totalAmount = req.body.totalAmount;
    const tip = req.body.tip;
    const discountPercentage = req.body.discountPercentage;
    const discountAmount = req.body.discountAmount;
    const promoCode = req.body.promoCode;
    const data = [userID, staffID, serviceID, transactionID, date,time,bill,tax,totalAmount,tip,discountPercentage,discountAmount,promoCode]
    const sql = "insert into bookings (userID, staffID, serviceID, transactionID, date,time,bill,tax,totalAmount,tip,discountPercentage,discountAmount,promoCode) values (?,?,?,?,?,?,?,?,?,?,?,?,?)"
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.put("/:id",(req,res)=>{
    const userID = req.body.userID;
    const staffID = req.body.staffID;
    const serviceID = req.body.serviceID;
    const transactionID = req.body.transactionID;
    const date = req.body.date;
    const time = req.body.time;
    const bill = req.body.bill;
    const tax = req.body.tax;
    const totalAmount = req.body.totalAmount;
    const tip = req.body.tip;
    const discountPercentage = req.body.discountPercentage;
    const discountAmount = req.body.discountAmount;
    const promoCode = req.body.promoCode;
    const bookingID= req.params.id;	
    const data = [userID, staffID, serviceID, transactionID, date,time,bill,tax,totalAmount,tip,discountPercentage,discountAmount,promoCode, bookingID]
    const sql = "update bookings set userID=?, staffID=?, serviceID=?, transactionID=?, date=?,time=?,bill=?,tax=?,totalAmount=?,tip=?,discountPercentage=?,discountAmount=?,promoCode=? where bookingID=?"
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.delete("/:id",(req,res)=>{
    con.query("delete from bookings where bookingID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

module.exports = router