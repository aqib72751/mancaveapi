const express = require('express');
const router = express.Router();
const con = require("../db")

router.get("/",(req,res)=>{
    con.query("select * from holidays", (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.get("/:id",(req,res)=>{
    con.query("select * from holidays where holidayID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result[0])
            res.send(result[0])
        }
    })
})

router.post("/",(req,res)=>{
    const date = req.body.date;
    const title = req.body.title;
    const message = req.body.message;

    const data = [date, title, message]
    const sql = "insert into holidays (date, title, message) values (?,?,?)"
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.put("/:id",(req,res)=>{
    const date = req.body.date;
    const title = req.body.title;
    const message = req.body.message;
    const holidayID= req.params.id;	
    const data = [date, title, message, holidayID]
    const sql = "update holidays set date=?, title=?, message=? where holidayID=?"
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.delete("/:id",(req,res)=>{
    con.query("delete from holidays where holidayID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

module.exports = router