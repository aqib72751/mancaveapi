const express = require('express');
const router = express.Router();
const con = require("../db")

router.get("/",(req,res)=>{
    con.query("select * from services", (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.get("/:id",(req,res)=>{
    con.query("select * from services where serviceID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result[0])
            res.send(result[0])
        }
    })
})

router.post("/",(req,res)=>{
    const name = req.body.name;
    const categoryID = req.body.categoryID;
    const cost = req.body.cost;
    const time = req.body.time;
    const sequence = req.body.sequence;
    const contact = req.body.contact;
    const data = [name, categoryID, cost, time, sequence,contact]
    const sql = "insert into services (name, categoryID, cost, time, sequence,contact) values (?,?,?,?,?,?)"
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.put("/:id",(req,res)=>{
    const name = req.body.name;
    const categoryID = req.body.categoryID;
    const cost = req.body.cost;
    const time = req.body.time;
    const sequence = req.body.sequence;
    const contact = req.body.contact;
    const serviceID= req.params.id;	
    const data = [name, categoryID, cost, time, sequence,contact, serviceID]
    const sql = "update services set name=?, categoryID=?, cost=?, time=?, sequence=?,contact=? where serviceID=?"
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.delete("/:id",(req,res)=>{
    con.query("delete from services where serviceID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

module.exports = router