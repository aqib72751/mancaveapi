const express = require('express');
const router = express.Router();
const con = require("../db")

router.get("/",(req,res)=>{
    con.query("select * from shopTimings", (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.get("/:id",(req,res)=>{
    con.query("select * from shopTimings where shopTimingID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result[0])
            res.send(result[0])
        }
    })
})

router.post("/",(req,res)=>{
    const day = req.body.day;
    const timeSlot = req.body.timeSlot;
    const data = [day, timeSlot]
    const sql = "insert into shopTimings (day, timeSlot) values (?,?)";
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.put("/:id",(req,res)=>{
    const day = req.body.day;
    const timeSlot = req.body.timeSlot;
    const shopTimingID= req.params.id;	
    const data = [day, timeSlot, shopTimingID]
    const sql = "update shopTimings set day=?, timeSlot=? where shopTimingID=?"
    con.query(sql,data, (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

router.delete("/:id",(req,res)=>{
    con.query("delete from shopTimings where shopTimingID=?",[req.params.id], (error, result)=>{
        if (error) {
            console.log(error)
            res.send(error)
        }else{
            console.log(result)
            res.send(result)
        }
    })
})

module.exports = router