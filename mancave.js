const express = require("express");
const mysql = require("mysql2");
const bodyParser = require("body-parser");
require("dotenv").config();
const port = process.env.PORT;
const app = express();
app.use(bodyParser.json());
const users = require("./routes/users");
const staff = require("./routes/staff");
const categories = require("./routes/categories");
const banners = require("./routes/banners");
const promoCodes = require("./routes/promoCodes");
const services = require("./routes/services");
const shopTimings = require("./routes/shopTimings");
const holidays = require("./routes/holidays");
const bookings = require("./routes/bookings");
const bookingDetails = require("./routes/bookingDetails");
const staffSpeciality = require("./routes/staffSpeciality");
const flags = require("./routes/flags");

const con = mysql.createConnection({
  database: process.env.DB_NAME,
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
});
con.connect((error, result) => {
  if (error) {
    console.log(error);
  } else {
    console.log("Databse connected");
    app.use("/users", users);
    app.use("/staff", staff);
    app.use("/categories", categories);
    app.use("/banners", banners);
    app.use("/promoCodes", promoCodes);
    app.use("/services", services);
    app.use("/shopTimings", shopTimings);
    app.use("/holidays", holidays);
    app.use("/bookings", bookings);
    app.use("/bookingDetails", bookingDetails);
    app.use("/staffSpeciality", staffSpeciality);
    app.use("/flags", flags);
  }
});

app.get("/", (req, res) => {
  res.send("ManCave App is running");
});
app.listen(port, () =>
  console.log("Man cave server is running on port ", port)
);
