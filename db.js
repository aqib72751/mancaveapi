var mysql = require("mysql2");
require("dotenv").config();
var db;

function connectDatabase() {
//   if (!db) {
//     db = mysql.createConnection({
//       host: process.env.DB_HOST,
//       user: process.env.DB_USER,
//       database: process.env.DB_NAME,
//       password: process.env.DB_PASS,
//     });

//     db.connect(function (err) {
//       if (!err) {
//         console.log("Database is connected!");
//       } else {
//         console.log("Error connecting database!");
//       }
//     });
//   }
//   return db;
// }

  const pool = mysql.createPool({
    host: process.env.DB_HOST,
      user: process.env.DB_USER,
      database: process.env.DB_NAME,
      password: process.env.DB_PASS,
  });

  return pool;
}

module.exports = connectDatabase();
